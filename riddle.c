#include <stdio.h>


struct DigitStruct{
    
    int zero;
    int one;
    int two;
    int three;
    int four;
    int five;
    int six;
    int seven;
    int eight;
    int nine;
    
};

typedef struct DigitStruct Digits;



void resetDigitCounter(Digits *digitCounter){
    
    
    digitCounter -> zero = 0;
    digitCounter -> one = 0;
    digitCounter -> two = 0;
    digitCounter -> three = 0;
    digitCounter -> four = 0;
    digitCounter -> five = 0;
    digitCounter -> six = 0;
    digitCounter -> seven = 0;
    digitCounter -> eight = 0;
    digitCounter -> nine = 0;
    
    
    
    return;
}



void enterDigit(int digit, Digits *digitCounter){
    
    
    if(digit == 0)
        (digitCounter -> zero)++;
    else if(digit == 1)
        (digitCounter -> one)++;
    else if(digit == 2)
        (digitCounter -> two)++;
    else if(digit == 3)
        (digitCounter -> three)++;
    else if(digit == 4)
        (digitCounter -> four)++;
    else if(digit == 5)
        (digitCounter -> five)++;
    else if(digit == 6)
        (digitCounter -> six)++;
    else if(digit == 7)
        (digitCounter -> seven)++;
    else if(digit == 8)
        (digitCounter -> eight)++;
    else if(digit == 9)
        (digitCounter -> nine)++;
    
    
    
    return;
}



void countDigits(int number, Digits *digitCounter){
    
    
    int digit_100 = number / 100;
    number = number - digit_100 * 100;
    
    int digit_10 = number / 10;
    number = number - digit_10 * 10;
    
    int digit_1 = number;
    
    
        
    enterDigit(digit_100, digitCounter);
    enterDigit(digit_10, digitCounter);
    enterDigit(digit_1, digitCounter);
    
    
    
    return;
}



void countDigits_5(int number, Digits *digitCounter){
    
    
    int digit_10000 = number / 10000;
    number = number - digit_10000 * 10000;
    
    int digit_1000 = number / 1000;
    number = number - digit_1000 * 1000;
    
    int digit_100 = number / 100;
    number = number - digit_100 * 100;
    
    int digit_10 = number / 10;
    number = number - digit_10 * 10;
    
    int digit_1 = number;
        
    
    
    enterDigit(digit_10000, digitCounter);
    enterDigit(digit_1000, digitCounter);    
    enterDigit(digit_100, digitCounter);
    enterDigit(digit_10, digitCounter);
    enterDigit(digit_1, digitCounter);
    
    
    
    return;
}



void computeAndCount(int factor_1, int factor_2, Digits *digitCounter){
    
    
    int intermediate_1 = 99;
    int intermediate_2 = 99;
    int intermediate_3 = 99;
    
    int result = 99;
    
    int factor_2_100 = factor_2 / 100;
    int factor_2_10 = (factor_2 - (factor_2_100 * 100)) / 10;
    int factor_2_1 = factor_2 - (factor_2_100 * 100) - (factor_2_10 * 10);
    
    
    
    intermediate_1 = factor_2_100 * factor_1;
    intermediate_2 = factor_2_10 * factor_1;
    intermediate_3 = factor_2_1 * factor_1;
    
    result = factor_1 * factor_2;
    
    
    
    if(intermediate_1 > 999 || intermediate_2 > 999 || intermediate_3 > 999)
        return;
    
    if(result < 10000 || result > 99999)
        return;
    
    
    
    countDigits(factor_1, digitCounter);
    countDigits(factor_2, digitCounter);
    countDigits(intermediate_1, digitCounter);
    countDigits(intermediate_2, digitCounter);
    countDigits(intermediate_3, digitCounter);
    countDigits_5(result, digitCounter);
    
    
    
    return;
}



int checkForCondition(Digits *digitCounter){
    
    
    int isTrue = 1;
    
    
    
    if(digitCounter -> zero != 2)
        isTrue = 0;
    
    if(digitCounter -> one != 2)
        isTrue = 0;
    
    if(digitCounter -> two != 2)
        isTrue = 0;
    
    if(digitCounter -> three != 2)
        isTrue = 0;
    
    if(digitCounter -> four != 2)
        isTrue = 0;
    
    if(digitCounter -> five != 2)
        isTrue = 0;
    
    if(digitCounter -> six != 2)
        isTrue = 0;
    
    if(digitCounter -> seven != 2)
        isTrue = 0;
    
    if(digitCounter -> eight != 2)
        isTrue = 0;
    
    if(digitCounter -> nine != 2)
        isTrue = 0;
    
    
    
    return isTrue;
}



int main(void){
    
    
    int notFound = 1;
    
    int factor_1 = 0;
    int factor_2 = 0;
    
    Digits digitCounter;
    resetDigitCounter(&digitCounter);
    
    
    
    while(notFound && factor_1 <= 999){
        
        
        for(factor_1 = 0; factor_1 <= 999; factor_1++){
            //printf("for 1 = %d", factor_1);
            
            for(factor_2 = 0; factor_2 <= 999; factor_2++){
                
                resetDigitCounter(&digitCounter);
                
                computeAndCount(factor_1, factor_2, &digitCounter);
                
                if(checkForCondition(&digitCounter))
                    notFound = 0;
                
                
                if(digitCounter.zero == 2 && digitCounter.one == 2 && digitCounter.two == 2 && digitCounter.three == 2 && digitCounter.four == 2 && digitCounter.five == 2 && digitCounter.six == 2 && digitCounter.seven == 2 && digitCounter.eight == 2 && digitCounter.nine == 2)
                    printf("%d  %d\n%d  %d  %d  %d  %d  %d  %d  %d  %d  %d\n", factor_1, factor_2, digitCounter.zero, digitCounter.one, digitCounter.two, digitCounter.three, digitCounter.four, digitCounter.five, digitCounter.six, digitCounter.seven, digitCounter.eight, digitCounter.nine);
                
                
            }
            
            
        }
        
        
    }
    
    
    
    if(!notFound){
        
        
        printf("\n");
        printf("factor 1 -> %d\n", factor_1);
        printf("factor 2 -> %d\n", factor_2);
        
        
    } else if(notFound){
        
        
        printf("\n");
        printf("There is no such facotrs!\n");
        
        
    }
    
    
    
    return 0;
}